#!/usr/bin/bash

#echo "Uninstalling"
#sudo rm -rf /usr/local/lib/python3.10/site-packages/yowe
#sudo rm -rf /usr/local/share/yowe
#sudo rm /usr/local/bin/yowe 

echo "Rebuilding"
#rm -rf _build/
meson --prefix /usr/local --reconfigure _build
meson _build
echo "Installing"
meson install -C _build

/usr/local/bin/yowe
