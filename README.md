Yowe
====
A mastodon client suitable for big and small screens.

It is pronounced: Yo:w_e_

It is NOT pronounced: Yow, Yo:wee

This is not a female sheep, all you etymologists, dig harder  :).

Licensed
========
GNU GPL v3+
