import sys
# Debian uses /usr/local/lib/python3/dist-packages/ which is not in the std path
sys.path.append("/usr/local/lib/python3/dist-packages/")
import pickle
import logging
from yowe.API.account import (Account, OAuthApp)
from yowe.config import YoweConfigParser

logging.basicConfig(level=logging.DEBUG)

config = YoweConfigParser()

acc, app = config.get_account()
if not acc:
    acc = Account("spaetz@mas.to")
    app = OAuthApp(acc.host)
    app.register()
    # not necessarily needed, app tokens rarely used
    #app.authorize()
    acc.set_oauth_app(app)
    acc.authorize()
    config.set_account(acc, app)
    config.write_config()

#app.verify()
acc.get_info_by_id("233446")
#print("TOKEN", acc.token["access_token"])
acc.search_accounts("spaetz")


