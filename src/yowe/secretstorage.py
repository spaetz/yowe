import gi
gi.require_version("Secret", "1")
from gi.repository import Secret
from yowe.const import Constants

SCHEMA = Secret.Schema.new(Constants.APP_ID, Secret.SchemaFlags.NONE,
                    {"full_id": Secret.SchemaAttributeType.STRING,
                     "id":  Secret.SchemaAttributeType.STRING,
                     "client_id": Secret.SchemaAttributeType.STRING,
                     "client_secret": Secret.SchemaAttributeType.STRING,
                     "app_token": Secret.SchemaAttributeType.STRING,
                     "user_token": Secret.SchemaAttributeType.STRING})


def on_password_stored(source, result, unused):
    Secret.password_store_finish(result)
    # ... do something now that the password has been stored

#save new password:
attributes = {
    "number": "8",
    "string": "eight",
    "even": "true"
}
Secret.password_store(SCHEMA, attributes, Secret.COLLECTION_DEFAULT,
                      "The label", "the password", None, on_password_stored)


#lookup password
def on_password_lookup(source, result, unused):
    password = Secret.password_lookup_finish(result)
    # password will be null, if no matching password found

Secret.password_lookup(EXAMPLE_SCHEMA, { "number": "8", "even": "true" },
                       None, on_password_lookup)
