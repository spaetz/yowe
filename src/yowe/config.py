import json
import logging
import os
from pathlib import Path
from configparser import ConfigParser

from yowe.API.account import (Account, OAuthApp)

class YoweConfigParser(ConfigParser):
    def __init__(self) -> None:
        super().__init__();

        """ Read the existing configuration """
        # path of config file
        xdg_config_dir: Path = Path(os.getenv("XDG_CONFIG_HOME", "~/.config"))\
            .expanduser()
        self._config_file: Path = xdg_config_dir / 'yowe/yowe.cfg'
        # Make config dir if it does not exist
        self._config_file.parent.mkdir(parents=True, exist_ok=True)
        if self._config_file.is_file():
            self.read_config()

    def read_config(self):
        logging.debug("Reading config file")
        self.read(self._config_file)

    def write_config(self):
        with open(self._config_file, 'w') as configfile:
            self.write(configfile)
        logging.debug("Wrote config file")

    def get_account(self):
        if not "account" in self.sections():
            return (None, None)
        account = self["account"]

        acc = Account(account["full_id"])
        app = OAuthApp(acc.host)
        acc.id = account.get("id", None)
        u_token = account.get("user_token", None)
        if u_token:
            acc.token = json.loads(u_token)
        acc.refresh_token = account.get("refresh_token", None)
        a_token = account.get("app_token", None)
        if a_token:
            app.token = json.loads(a_token)
        app.client_id = account.get("client_id", None)
        app.client_secret = account.get("client_secret", None)
        # We can connect acc to app, after all credentials have been set.
        acc.set_oauth_app(app)

        return (acc, app)

    def set_account(self, acc, app):
        # remove preexistig configs
        self.remove_section("account")
        self.add_section("account")
        account =  self["account"]
        if acc.id is not None:
            account["id"] = acc.id
        if acc.full_id is not None:
            account["full_id"] = acc.full_id
        if acc.token is not None:
            account["user_token"] = json.dumps(acc.token)
        if acc.refresh_token is not None:
            account["refresh_token"] = acc.refresh_token
        if app.token is not None:
            account["app_token"] = json.dumps(app.token)
        if app.client_id is not None:            
            account["client_id"] = app.client_id
        if app.client_secret is not None:
            account["client_secret"] = app.client_secret
