import logging
import requests
import urllib.parse

from oauthlib.oauth2 import MobileApplicationClient
from requests_oauthlib import OAuth2Session
from typing import (Optional, List)

from yowe.const import Constants


class OAuthApp():
    """ Register an app with the OAUTH provider

    and retrieve the response, such as
    {
    "id": "563419",
    "name": "test app",
    "website": null,
    "redirect_uri": "urn:ietf:wg:oauth:2.0:oob",
    "client_id": "TWhM-tNSuncnqN7DBJmoyeLnk6K3iJJ71KKXxgL1hPM",
    "client_secret": "ZEaFUFmF0umgBX1qKJDjaU99Q31lDkOU8NutzTOoliw",
    "vapid_key": "BCk-QqERU0q-CfYZjcuB6lnyyOYfJ2AifKqfeGIm7Z-HiTU5T9eTG5GxVA0_OH5mMlI4UkkDTpaZwozy0TzdZ2M="
    }
    It stores the information for usage with the client
    """

    DEF_HEADERS = {'User-Agent': Constants.APP_ID}
    class SCOPES:
        """ Some oauth scopes that we can request access to.
        Mastodon assumes read if left empty
        """
        DEFAULT = ""
        READ    = "read"
        WRITE   = "write"
        FULL    = "read write follow"
        
    class API:
        """ API endpoints """
        REGISTER: str  = "/api/v1/apps"
        VERIFY: str    = "/api/v1/apps/verify_credentials"
        GET_TOKEN: str = "/oauth/token"


    def __init__(self, base_url: str) -> None:
        # base scheme+netloc of the instance
        self.base_url = base_url
        # result of client registration
        self.id: str = None
        # Uri to redirect to after authorization, can be
        # "urn:ietf:wg:oauth:2.0:oob" for Out-of-Band
        self.redirect_uris: str = "http://localhost:15858"
        self.client_name: str  = Constants.APP
        self.website: str      = Constants.HOME_URL
        # result of client registration
        self.client_id: Optional[str] = None
        # result of client registration
        self.client_secret: Optional[str] = None
        # result of client registration, identifies server
        self.vapid_key: Optional[str] = None
        # A app-level token, if we have one
        # {"access_token":"sO3cPnJF3Ga9x-LpDMQQ6x2MU12cCrG2w1cBM7Cj-1k","token_type":"Bearer","scope":"read","created_at":1665851191}
        self.token: Optional[str]     = None

    def register (self, scopes: str = SCOPES.FULL) -> None:
        """Register a new client with the OAUTH provider

        :param scopes: Scopes that 
        Form data parameters that can be sent:
        client_name: string A name for your application
        redirect_uris: string Where the user should be redirected after authorization. To display the authorization code to the user instead of redirecting to a web page, use urn:ietf:wg:oauth:2.0:oob in this parameter.
        scopes: (optional) strin Space separated list of scopes. If none is provided, defaults to read.
        website: (optional) A URL to the homepage of your app

        :returns: True on success. Can raise ValueError if preconditions are
                  not fulfilled.

        """
        if self.client_secret and self.client_id:
            logging.info("App is already registered, no need to re-register")
            return True
        if not self.base_url:
            raise ValueError("Cannot register app with empty base URL, "
                             "bad things will happen")
        register_uri = self.base_url + self.API.REGISTER
        data = {"client_name": self.client_name,
                "website": Constants.HOME_URL,
                "scopes": scopes,
                "redirect_uris": self.redirect_uris}
        response = requests.post(register_uri,
                                headers = OAuthApp.DEF_HEADERS,
                                data    = data)
        # 422 implies a required parameter is missing or improperly formatted.
        if response.status_code == 422:
            raise ValueError(f"OAuth app registration failed: {response.json()['error']}")
        if not response:
            logging.error(f"Failed to register client: "\
                          "({response.status_code}): reponse.text")
            return False
        
        # success
        res = response.json()
        self.id = res["id"]
        self.client_id = res["client_id"]
        self.client_secret = res["client_secret"]
        self.vapid_key = res["vapid_key"] 
        logging.info(f"Successfully registered client under id {self.id} with sopes {scopes}")
        return True


    def authorize(self):
        """ Authorize and fetch app access token """
        import webbrowser, os
        if not self.base_url:
            raise ValueError("Cannot perform operationempty base URL, "
                             "bad things will happen")
        url = self.base_url + self.API.GET_TOKEN
        data = {"grant_type": "client_credentials",
                "client_id": self.client_id,
                "client_secret": self.client_secret,
                "scope": self.SCOPES.FULL,
                "redirect_uri": "urn:ietf:wg:oauth:2.0:oob"}
        response = requests.post(url,
                                 header = OAuthApp.DEF_HEADERS,
                                 data   = data)
        if not response:
            logging.error("Retrieving app token failed")
            return False
        self.token = response.json()
        return True


    def verify(self):
        """Verify existing credentials are valid
        :returns: True on success, False on wrong credentials
        """
        if not (self.client_secret and self.client_id):
            logging.error("App is not registered yet, cannot verify")
            return False
        if not self.base_url:
            raise ValueError("Cannot verify app with empty base URL, "
                             "bad things will happen")
        if not self.token:
            raise ValueError("There is no app-level token yet. We cannot verify. Get one first")

        verify_uri = self.base_url + self.API.VERIFY
        headers = {"Authorization": "Bearer {}".format(self.token["access_token"])}
        headers.update(OAuthApp.DEF_HEADERS)
        response = requests.get(verify_uri,
                                headers = headers)
        if not response:
            logging.warn("Failed to verify app credentials {response.json()}")
            return False
        return True



class Account():
    class API:
        AUTHORIZE: str = "/oauth/authorize"
        GET_TOKEN: str = "/oauth/token"
        REVOKE_TOKEN: str= "/oauth/revoke"

        REGISTER    = "/api/v1/accounts"
        VERIFY      = "/api/v1/accounts/verify_credentials"
        UPDATE_CRED = "/api/v1/accounts/update_credentials"
        GET_INFO    = "/api/v1/accounts/{id}"
        GET_STATUSES= "/api/v1/accounts/:id/statuses"
        GET_FOLLOWERS="/api/v1/accounts/:id/followers"
        GET_FOLLOWING="/api/v1/accounts/:id/following"
        GET_FEATURED_TAG="/api/v1/accounts/:id/featured_tags"
        GET_LISTS   = "/api/v1/accounts/:id/lists"
        GET_ID_PROOFS="/api/v1/accounts/:id/identity_proofs"

        FOLLOW      = "/api/v1/accounts/:id/follow"
        UNFOLLOW    = "/api/v1/accounts/:id/unfollow"
        BLOCK       = "/api/v1/accounts/:id/block"
        UNBLOCK     = "/api/v1/accounts/:id/unblock"
        MUTE        = "/api/v1/accounts/:id/mute"
        UNMUTE      = "/api/v1/accounts/:id/unmute"
        FEATURE_PROFILE = "/api/v1/accounts/:id/pin"
        UNFEATURE_PROFILE = "/api/v1/accounts/:id/unpin"
        SET_PRIV_NOTE= "/api/v1/accounts/:id/note"
        GET_RELATIONSHIPS ="/api/v1/accounts/relationships"
        SEARCH_ACC  = "/api/v1/accounts/search"

    def __init__(self, full_id: str) -> None:
        """
        :param:
          full_id: User id in the form "username@domain"
        """
        # "id" of account in db, usually a number as str
        self.id: Optional[str]             = None
        # Username ("spaetz")
        self.local_id: str                 = full_id.split("@")[0]
        # User domain ("mas.to")
        self.domain_id: str                = full_id.split("@")[1]
        # User's "application/activity+json" URL
        self.id_url: str                   = ""
        # Base instance hostname (scheme+netloc)
        self.host: Optional[str]           = None
        self.oauth_app: Optional[OAuthApp] = None
        # OAuthApp (ie registered client) to be used for actions
        self.oauthapp: Optional[OAuthApp]  = None
        # User token (Bearer) after authorization
        # Struct Token:
        # {'access_token': 'DMlb-l3qqYkcvQeEJP1yp3o0CJiFCS9D82_pIjTLrzY', 'token_type': 'Bearer', 'scope': ['read'], 'created_at': 1665844300}
        self.token: Optional[Dict]          = None
        self.refresh_token: Optional[str]   = None
        # Contains OAuth session once the oauth app has been set
        self.oauth: Optional[OAuth2Session] = None
        self.find_hostname()

    @property
    def full_id(self) -> str:
        return "{}@{}".format(self.local_id, self.domain_id)

    def set_oauth_app(self, oauthapp: OAuthApp) -> None:
        """Set oauthapp to be used for actions

        We need a registered client for retrieving access tokens,
        so this needs to be set for this."""
        self.oauthapp = oauthapp
        assert self.oauthapp.client_id, "No client id retrieved yet, "\
                                        "register your client first"
        scopes = OAuthApp.SCOPES.FULL
        self.oauth = OAuth2Session(self.oauthapp.client_id,
                                   redirect_uri=self.oauthapp.redirect_uris,
                                   scope=scopes)


    def find_hostname(self):
        """Checks .well-known and DNS for real host names
        
        Run automatically on instantiation. Searches the base
        hostname, based on the user id. It sets self.host on success.
        :returns: True on success, and False on failure """
        #TODO: catch all exceptions and simply return False with some
        #      logging then
        from yowe.API.webfinger import WebFinger

        finger = WebFinger("https://" + self.domain_id)
        finger.query(self.full_id)
        id_url = finger.get_activitypub_id_url()
        if not id_url:
            logging.warn("Failed to get id_url through webfinger")
            return False
        self.id_url = id_url
        res = urllib.parse.urlparse(self.id_url)
        self.host = res[0] + "://" + res[1]
        logging.debug(f"Found Hostname {self.host} through webfinger.")
        return True

    def authorize(self):
        """ Authorizes user and fetches access token """
        import webbrowser
        assert self.host, "Cannot perform operation on empty base URL"

        url = self.host + self.API.AUTHORIZE
        auth_url, state = self.oauth.authorization_url(url)
        
        logging.info('Opening auth url %s' % auth_url)
        webbrowser.open (auth_url)

        from http.server import HTTPServer, BaseHTTPRequestHandler
        class html(BaseHTTPRequestHandler):
            import urllib.parse
            def do_GET (self):
                # /?code=2t7fXj6uaKlGMLGHpUCXjxltBKKP_aL82nDg_5Z2YsQ&state=oeY0HwAQzOi6tWl6pS0rcgdl4swHih
                query = urllib.parse.parse_qs( urllib.parse.urlparse(self.path).query )
                self.server.code = query.get("code", [])[0]
                self.send_response (200)
                self.send_header ('python 3','text/html')
                self.end_headers ()
                self.wfile.write (bytes("All Done. You can close this window now.", "utf8"))

        with HTTPServer (('localhost', 15858), html) as server:
            server.code = None
            server.handle_request()
            self.refresh_token = server.code
        logging.info(f"OAUth code: {self.refresh_token}")

        url = self.host + self.API.GET_TOKEN
        scopes = OAuthApp.SCOPES.FULL
        token = self.oauth.fetch_token(
            url,
            code=self.refresh_token,
            scope=scopes,
            client_secret=self.oauthapp.client_secret)
        print("TOKEN",token)
        self.token = token
        return True


    def register(self):
        """
        #header: Authorization Bearer <app token>
        #Form Data Parameters
        :username: string The desired username for the account
        :email: string The email address to be used for login
        :password: string The password to be used for login
        :agreement: boolean Whether the user agrees to the local rules, terms, and policies.
        :locale: string The language of the confirmation email that will be sent
        :reason: optional string Text that will be reviewed by moderators if registrations require manual approval.
        """
        pass

    def verify():
        pass

    def update_cred():
        pass

    def get_info_by_id(self, id: str=None):
        """Get info about the account user based on 'id'"""
        if not self.host:
            raise ValueError("Cannot perform operationempty base URL, "
                             "bad things will happen")
        getinfo_url = self.host + self.API.GET_INFO.format(id=id)
        response = requests.get(getinfo_url,
                                headers = OAuthApp.DEF_HEADERS)

        print("GETINFO", response, response.json(), response.request.url)

    def get_statuses():
        pass

    def get_follower():
        pass

    def get_following():
        pass

    def search_accounts(self, q: str):
        """Search for matching accounts by username or display name.
        auth: User token + read:accounts"""
        if not self.host:
            raise ValueError("Cannot perform operationempty base URL, "
                             "bad things will happen")
        if not self.token:
            raise ValueError("No user token available")
        url = self.host + self.API.SEARCH_ACC
        data = {"q": q,
                # Attempt WebFinger lookup. Defaults to false. Use
                # this when q is an exact address.
                #"resolve": "false"
                }
        #resolve optional string
        header = {"Authorization": "Bearer {}".format(self.token["access_token"])}
        header.update(OAuthApp.DEF_HEADERS)
        response = requests.get(url,
                                data = data,
                                headers = header)

        print("SEARCHACC", response, response.json())

