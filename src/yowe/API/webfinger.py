import logging
import requests

from typing import (Dict)

from yowe.const import Constants

class WebFinger:
    """Get mastodon user's instance and location via WebFinger"""

    API_URL = "/.well-known/webfinger"
    DEF_HEADERS = {'User-Agent': Constants.APP_ID}

    def __init__(self, base_uri: str) -> None:
        """
        :base_uri: scheme & hostname to be used for querying
        """
        self.base_uri: str = base_uri
        self._data: Optional[Dict] = None

    def query(self, id: str) -> bool:
        """queries the user id to see if there is a webfinger for the user

            :returns: False if not found, and true if found
        """
        logging.debug(f"Querying webfinger for user {id}")

        response = requests.get(self.base_uri + WebFinger.API_URL,
                                headers = WebFinger.DEF_HEADERS,
                                params  = {'resource': f"acct:{id}"})
        if (not response):
            logging.info(f"Webfinger for URL {response.request.url} failed"
                         "with code {response.status_code}")
            return False

        #response.status_code in 2XX range
        self._data: Optional[Dict] = response.json()
        return True

    def get_activitypub_id_url(self):
        """Returns the URI of the activitpub user that was last queried

        id_url, such as e.g. https://mastodon.social/users/Gargron
        :returns: (string) user_url or "" on failure
        """
          
        if not self._data:
            errstr = "get_activitypub_base_url failed: Seems we have not yet successfully queried a user"
            logging.warning(errstr)
            raise Exception(errstr)

        links = self._data.get("links", None)
        for link in links:
            if link.get("rel", None) == "self" and \
               link.get("type", None) == "application/activity+json":
                if not link.get("href", None):
                    logging.warn(".well-known seems broken, no HREF present")
                    return ""
                return link["href"]
        return ""

    def get_activitypub_id(self):
        """Returns the id of the activitpub user that was last queried

        id is usually a number in string form
        :returns: (string) id or "" on failure
        """
        if not self._data:
            errstr = "get_activitypub_id failed: Seems we have not yet successfully queried a user"
            logging.warning(errstr)
            raise Exception(errstr)

        links = self._data.get("links", None)
        for link in links:
            if link.get("rel", None) == "self" and \
               link.get("type", None) == "application/activity+json":
                if not link.get("href", None):
                    logging.warn(".well-known seems broken, no HREF present")
                    return ""
                return link["href"]
        return ""
