from yowe.const import Constants
import os
from pathlib import Path
import os.path
from configparser import ConfigParser

class Configuration:
    def __init__(self) -> None:
        """ Read the existing configuration """
        self.config: Configparser = ConfigParser()
        # path of config file
        xdg_config_dir: Path = Path(os.getenv("XDG_CONFIG_HOME", "~/.config"))\
            .expanduser()
        self.config_file: Path = xdg_config_dir / 'yowe/yowe.cfg'
        # Make config dir if it does not exist
        Path.mkdir(parents=True, exist_ok=True)
        if self.config_file.is_file():
            self.read_config(self.config_file)

    def read_config(self):
        self.config.read(self.config_file)

    def write_config(self):
        with open(self.config_file, 'w') as configfile:
            self.config.write(configfile)

def return_accounts():
    
class Account:
    pass
